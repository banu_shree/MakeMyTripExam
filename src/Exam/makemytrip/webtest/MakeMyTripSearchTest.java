package Exam.makemytrip.webtest;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Exam.makemytrip.webmethod.MakeMyTripMethod1;
import btac.sa.lib.BrowserActions;
import btac.sa.lib.MouseAndKeyboard;
import btac.sa.lib.SelectBrowser;
import btac.sa.lib.WebCommonMethods;
	

public class MakeMyTripSearchTest extends SelectBrowser
{
	WebCommonMethods general;
	MakeMyTripMethod1 search;
	BrowserActions ba;
	MouseAndKeyboard mk;

	String appserver;
	
	@BeforeMethod
    public void openTheBrowser() throws Exception 
    {
    	WebDriver d = getBrowser();
	    search = PageFactory.initElements(d, MakeMyTripMethod1.class);
	    general = PageFactory.initElements(d, WebCommonMethods.class);// initiating the driver and the .class file (the pageObject script)	    
	    ba = PageFactory.initElements(d, BrowserActions.class);// initiating the driver and the .class file (the pageObject script)	    
	    mk = PageFactory.initElements(d, MouseAndKeyboard.class);// initiating the driver and the .class file (the pageObject script)	    

	    BrowserActions.openURLBasedOnDbDomain();
    } 

	//validLogin test 
	@Test(priority=1, groups={"search"})
	public void validLoginCheck() throws Exception
	{	
		System.out.println("************************Search started***************************");
		MakeMyTripMethod1.validLogin("mytrip");
		//LoginLogoutMethods.logout();
		System.out.println("++++++++++++++++++++++++Search success+++++++++++++++++++++++++++");
	}

	
	@AfterMethod(alwaysRun=true)
    public void catchExceptions(ITestResult result) throws Exception 
    {    
    	String methodname = result.getName();
        if(!result.isSuccess()){            
        	WebCommonMethods.screenshot(methodname);
        }
        BrowserActions.quit(); // Calling function close to quit browser instance
    }

}
