package Exam.makemytrip.webmethod;



import java.io.File;

import jxl.Cell;
import jxl.LabelCell;
import jxl.Sheet;
import jxl.Workbook;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import btac.sa.lib.FilesAndFolders;
import btac.sa.lib.MouseAndKeyboard;
import btac.sa.lib.SelectBrowser;
import btac.sa.lib.WebCommonMethods;

public class MakeMyTripMethod1  extends SelectBrowser {

	public static WebDriver driver;
	public MakeMyTripMethod1(WebDriver driver){
		this.driver=driver;
	}

	static Workbook wrk1;
	static Sheet sheet1;
	static Cell colRow;


	
	@FindBy(xpath = ".//*[@id='header_tab_hotels']")  //or "._39M2dM:nth-of-type(1)"  or  "._39M2dM:first-child"
	public static WebElement hotelTab;
	
	@FindBy(xpath = ".//*[@id='js-hp-widgetSection']/h1")  //or "._39M2dM:nth-of-type(1)"  or  "._39M2dM:first-child"
	public static WebElement hotelHeading;
	
	@FindBy(xpath= ".//*[@id='hp-widget__sDest']")  //or "._39M2dM:nth-of-type(1)"  or  "._39M2dM:first-child"
	public static WebElement cityField;
	
	
	
	@FindBy(xpath= "html/body/div[4]/div[1]/section/div/div[2]/input")  //or "._39M2dM:nth-of-type(1)"  or  "._39M2dM:first-child"
	public static WebElement checkinField;
	
	@FindBy(xpath= "html/body/div[4]/div[2]/div/div[2]/div/div[1]/table/tbody/tr[4]/td[5]/a") 
	public static WebElement fromDate;
	
	
	@FindBy(xpath= "html/body/div[4]/div[1]/section/div/div[3]/input")  //or "._39M2dM:nth-of-type(1)"  or  "._39M2dM:first-child"
	public static WebElement checkoutField;
	
	@FindBy(xpath= "html/body/div[4]/div[2]/div/div[3]/div/div[1]/table/tbody/tr[4]/td[7]/a")  //or "._39M2dM:nth-of-type(1)"  or  "._39M2dM:first-child"
	public static WebElement toDate;
	
	
	@FindBy(xpath = ".//*[@id='hp-widget__paxCounter']")
	public static WebElement RoomClick;
	
	@FindBy(xpath = ".//*[@id='js-adult_counter']/li[6]")
	public static WebElement AdultCheck;
	
	@FindBy(xpath = ".//*[@id='js-addGuestRoom']")
	public static WebElement AddRoom;

	@FindBy(xpath = ".//*[@id='js-filterOptins']/div/div[4]/p/a")
	public static WebElement Done;
	
	
	@FindBy(xpath = ".//*[@id='searchBtn']")
		public static WebElement searchButton;
	
	
	
	
	@FindBy(xpath = "html/body/page-view/div/div[3]/div[3]/div[7]/header/div/div[1]/h1/span")
	public static WebElement AssertHotel;
	


	


	public static void validLogin(String userName ) throws Exception
	{
		
		Thread.sleep(2000);
		WebCommonMethods.callingImplicitSleep();	
		hotelTab.click();
		Thread.sleep(2000);
		WebCommonMethods.callingImplicitSleep();
		
		Thread.sleep(3000);
		String hmsg=hotelHeading.getText();
		Assert.assertEquals(hmsg, "Book Domestic and International hotels");
		Thread.sleep(5000);
		System.out.println(hmsg+"------------------------------------------------------------");
		

			cityField.clear();
			cityField.sendKeys("ooty");
			Thread.sleep(2000);
			
			
			checkinField.click();
			Thread.sleep(2000);
			
			fromDate.click();
			Thread.sleep(3000);
			
			
			checkoutField.click();
			Thread.sleep(2000);
			
			toDate.click();
			Thread.sleep(3000);
			
			RoomClick.click();
			Thread.sleep(3000);
			
			AdultCheck.click();
			Thread.sleep(3000);
			
			AddRoom.click();
			Thread.sleep(3000);
			
			Done.click();
			Thread.sleep(3000);


			searchButton.click();
			Thread.sleep(5000);
			WebCommonMethods.callingImplicitSleep();
			
			
		
			String data1=AssertHotel.getText();
			Assert.assertEquals(data1, "Hotels in Ooty");
			Thread.sleep(2000);

		
	}


}
